# Reactr demo

## Create and build a Runnable

```bash
# create the runnable
subo create runnable hello --lang tinygo
# build the runnable
subo build hello/
```

## Build the Runnable launcher

```bash
go build
```
