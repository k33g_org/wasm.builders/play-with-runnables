package main

import (
	"fmt"
	"github.com/suborbital/reactr/rt"
	"github.com/suborbital/reactr/rwasm"
)

func main() {
	r := rt.New()
	doHelloWasm := r.Register("hello-world", rwasm.NewRunner("./hello-world/hello-world.wasm"))

	result, err := doHelloWasm("Bob Morane!").Then()
	if err != nil {
		fmt.Println(err)
		return
	}
	
	fmt.Println(string(result.([]byte)))

}
