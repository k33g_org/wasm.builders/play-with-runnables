package main

import (
	"log"

	"github.com/suborbital/reactr/api/tinygo/runnable"
	reactrHttp "github.com/suborbital/reactr/api/tinygo/runnable/http"
)

type HelloWorld struct{}

func (h HelloWorld) Run(input []byte) ([]byte, error) {
	payload, _ := reactrHttp.GET("https://jsonplaceholder.typicode.com/todos/1", nil)
	log.Println(string(payload))
	return []byte("Hello, " + string(input)), nil
}

// initialize runnable, do not edit //
func main() {
	runnable.Use(HelloWorld{})
}
